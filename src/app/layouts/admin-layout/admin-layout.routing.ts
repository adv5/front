import {Routes} from '@angular/router';

import {DashboardComponent} from '../../pages/dashboard/dashboard.component';
import {IconsComponent} from '../../pages/icons/icons.component';
import {MapComponent} from '../../pages/map/map.component';
import {NotificationsComponent} from '../../pages/notifications/notifications.component';
import {UserComponent} from '../../pages/user/user.component';
import {TablesComponent} from '../../pages/tables/tables.component';
import {TypographyComponent} from '../../pages/typography/typography.component';
import {IncidentsComponent} from '../../pages/incidents/incidents.component';
import {MyIncidentsComponent} from '../../pages/my-incidents/my-incidents.component';
import {AuthComponent} from '../../pages/auth/auth.component';
import {CreateIncidentComponent} from '../../pages/create-incident/create-incident.component';
import {RegisterComponent} from '../../pages/register/register.component';

export const AdminLayoutRoutes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  {path: 'create-incident', component: CreateIncidentComponent},
  {path: 'auth', component: AuthComponent},
  {path: 'icons', component: IconsComponent},
  {path: 'my-incidents', component: MyIncidentsComponent},
  {path: 'incidents', component: IncidentsComponent},
  {path: 'incident/:id', component: MapComponent},
  {path: 'notifications', component: NotificationsComponent},
  {path: 'user', component: UserComponent},
  {path: 'tables', component: TablesComponent},
  {path: 'typography', component: TypographyComponent},
  {path: 'register', component: RegisterComponent},
];
