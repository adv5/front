import {Component, OnInit} from '@angular/core';

declare interface RouteInfo {
  path: string;
  title: string;
  rtlTitle: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  {
    path: '/dashboard',
    title: 'Dashboard',
    rtlTitle: 'لوحة القيادة',
    icon: 'icon-chart-pie-36',
    class: ''
  },
  {
    path: '/incidents',
    title: 'Incidents',
    rtlTitle: 'خرائط',
    icon: 'icon-bullet-list-67',
    class: ''
  },
  {
    path: '/my-incidents',
    title: 'My incidents',
    rtlTitle: 'خرائط',
    icon: 'icon-chat-33',
    class: ''
  },
  {
    path: '/create-incident',
    title: 'Create incident',
    rtlTitle: 'إخطارات',
    icon: 'icon-cloud-upload-94',
    class: ''
  },
  {
    path: '/user',
    title: 'User Profile',
    rtlTitle: 'ملف تعريفي للمستخدم',
    icon: 'icon-single-02',
    class: ''
  }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() {
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }

  isMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  }
}
