import {Component, OnInit} from '@angular/core';
import {Service} from '../services/service';

declare const google: any;

@Component({
  selector: 'app-my-incidents',
  templateUrl: './my-incidents.component.html',
  styleUrls: ['./my-incidents.component.scss']
})
export class MyIncidentsComponent implements OnInit {
  incidents: any[] = [];

  constructor(private service: Service) {
  }

  ngOnInit(): void {

    this.service.getMyIncidents().subscribe(res => {
      this.incidents = res
    });

  }
  getColor(incident: any) {
    if (incident.incidentStatus === 'DONE') {
      return 'success'
    }
    if (incident.rank === 3) {
      return 'danger'
    } else if (incident.rank === 2) {
      return 'warning'
    } else {
      return 'info'
    }
  }


}
