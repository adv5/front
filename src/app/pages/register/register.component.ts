import {Component, OnInit} from '@angular/core';
import {Service} from '../services/service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html',
  styleUrls: ['./register.component.scss']

})
export class RegisterComponent implements OnInit {
  user = new User();

  constructor(private service: Service,
              private router: Router) {
  }

  ngOnInit() {
  }

  login() {
    this.router.navigate(['/auth']);
  }

  register() {
    this.user.birthdayDate = new Date();
    this.service.createUser(this.user).subscribe(res => {
      if (res !== 'Error') {
        localStorage.setItem('userId', res);
        this.router.navigate(['/auth']);
      }

    })
  }
}

class User {
  login: string;
  password: string;
  firstName: string;
  lastName: string;
  phone: string;
  address: string;
  birthdayDate: any;
}
