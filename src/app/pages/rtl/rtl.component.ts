import {Component, OnInit} from '@angular/core';
import Chart from 'chart.js';
import {Service} from '../services/service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-rtl',
  templateUrl: 'rtl.component.html'
})
export class RtlComponent implements OnInit {
  username: any;
  password: any;

  constructor(private service: Service,
              private router: Router) {
  }

  ngOnInit() {
    this.router.navigate(['asdasdasd']);
  }

  login() {
    this.service.login(this.username, this.password).subscribe(res => {
      if (res !== 'Error') {
        localStorage.setItem('userId', res)
        this.router.navigate(['/dashboard']);
      }

    })
  }
}
