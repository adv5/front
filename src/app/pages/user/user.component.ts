import {Component, OnInit} from '@angular/core';
import {Service} from '../services/service';

@Component({
  selector: 'app-user',
  templateUrl: 'user.component.html'
})
export class UserComponent implements OnInit {
  user: any;

  constructor(private service: Service) {
  }

  ngOnInit() {
    this.service.getUser().subscribe(res => {
      this.user = res;
    })
  }
}
