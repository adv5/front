import { Component, OnInit } from '@angular/core';
import {Service} from "../services/service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  username: any;
  password: any;

  constructor(private service: Service,
              private router: Router) { }

  ngOnInit(): void {
  }

  login() {
    this.service.login(this.username, this.password).subscribe(res => {
      if (res !== 'Error') {
        localStorage.setItem('userId', res)

        this.router.navigate(['/dashboard']);

      }

    })
  }

  register() {
    this.router.navigate(['/register']);
  }
}
