import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class Service {

  private readonly PUBLIC_AUTHENTICATE = '';

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  getIncidents(): Observable<any> {
    return this.httpClient.get('/back/incidents');
  }

  getMyIncidents(): Observable<any> {
    const userID = localStorage.getItem('userId') || '1';

    return this.httpClient.get('/back/incident/user/' + userID);
  }

  getIncident(id: any): Observable<any> {
    return this.httpClient.get('/back/incident/' + id);
  }

  createIncident(object: any): Observable<any> {
    return this.httpClient.post('/back/createIncident', object);
  }

  take(id: any): Observable<any> {
    const userID = localStorage.getItem('userId') || '1';
    return this.httpClient.get('/back/incident/' + id + '/' + userID);
  }

  login(username: string, password: string): Observable<any> {
    return this.httpClient.get(`/back/loginUser?login=${username}&password=${password}`);
  }

  createUser(object: any): Observable<any> {
    return this.httpClient.get(`/createUser`, object);
  }

  logout(): void {
    this.resetLocalVariables();
    this.router.navigate(['/']);
  }

  private resetLocalVariables() {
    localStorage.removeItem('userId');
  }

  done(id: any): Observable<any> {
    return this.httpClient.get('/back/incident/done/' + id);

  }

  getUser(): Observable<any> {
    const userID = localStorage.getItem('userId') || 'null';

    return this.httpClient.get('/back/user/' + userID);

  }
}

