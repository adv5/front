import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {ToastrModule} from 'ngx-toastr';

import {AppComponent} from './app.component';
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth-layout/auth-layout.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppRoutingModule} from './app-routing.module';
import {ComponentsModule} from './components/components.module';
import {IncidentsComponent} from './pages/incidents/incidents.component';
import {MyIncidentsComponent} from './pages/my-incidents/my-incidents.component';
import { AuthComponent } from './pages/auth/auth.component';
import { CreateIncidentComponent } from './pages/create-incident/create-incident.component';
import { RegisterComponent } from './pages/register/register.component';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    ToastrModule.forRoot()
  ],
  declarations: [AppComponent, AdminLayoutComponent, AuthLayoutComponent, IncidentsComponent, MyIncidentsComponent, AuthComponent, CreateIncidentComponent, RegisterComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
